# Short helm howto
(https://rimusz.net/tillerless-helm)
This assumes you use helm v2 - has not been tested with v3 yet.
## Recommended steps
* Install the tiller plugin that lets you run tiller outside of the cluster:
  helm plugin install https://github.com/rimusz/helm-tiller
* Using the plugin
** list installed charts
    `helm tiller run <my-team-namespace> -- helm list`
** install the charts (assuming your cwd is where this README is)
    You need to be logged in with an administrative account because the Prometheus chart installs a RoleBinding for the discovery permissions.
    `helm tiller run promdemo -- helm install --name promdemo .`
** delete the installation
    `helm tiller run promdemo -- helm delete --purge promdemo`
## Grafana
* Grafana will be available at nodePort 32000.
* The password for the 'admin' user is 'grafana'.
## Prometheus
* Prometheus will be available at nodePort 30090.
