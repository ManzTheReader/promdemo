package main

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gopkg.in/yaml.v2"
	"io"
	"log"
	"math"
	"math/rand"
	"net/http"
	"os"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"time"
)

var (
	metricTypes      = []string{"gauge", "counter", "summary", "histogram"} // untyped metrics not supported yet
	verbose          = log.New(os.Stdout, "INFO ", log.LstdFlags)
	verbosecnt       = 0  // to reduce output
	verbosefrequence = 30 // every 30 seconds
	configChanged    = true
)

type config struct {
	Labels  labels  `yaml:"labels"`
	Metrics metrics `yaml:"metrics"`
}

type label struct {
	Name   string   `yaml:"name"`
	Values []string `yaml:"values"`
}

func (l *label) validate() error {
	if l.Name == "" {
		return errors.New("found label without a 'name'")
	} else if len(l.Values) == 0 {
		return errors.New(fmt.Sprintf("label '%s' without values", l.Name))
	}
	return nil
}

// areLabelsEqual
// structs containing []string cannot be compared using ==
func (l1 *label) isEqualTo(l2 *label) bool {
	return l1.Name == l2.Name &&
		strings.Join(l1.Values, ",") == strings.Join(l2.Values, ",")
}

type labels []*label

func (l labels) names() []string {
	ret := make([]string, len(l))
	for i := range l {
		ret[i] = l[i].Name
	}
	sort.Strings(ret)
	return ret
}

func (l1 labels) areEqualTo(l2 labels) bool {
	if len(l1) != len(l2) {
		return false
	}
	for i, label := range l1 {
		if label.Name != l2[i].Name ||
			strings.Join(label.Values, ";") != strings.Join(l2[i].Values, ";") {
			return false
		}
	}
	return true
}

func (l labels) validate() error {
	for _, label := range l {
		if err := label.validate(); err != nil {
			return err
		}
	}
	return nil
}

func (l labels) sortValues() {
	for _, label := range l {
		sort.Strings(label.Values)
	}
}

// combinations
// returns a labelCombinator object which holds all value combinations acc. to the selected labels
// remember: wantedLabelNames is sorted, so use this as order
func (l1 labels) combinations(l2 labels, wantedLabelNames []string, replicas uint) *labelCombinator {
	available := append(l1, l2...)
	instanceids := &label{"instanceid", make([]string, replicas)}
	for i := 1; i <= int(replicas); i++ {
		instanceids.Values[i-1] = "replica-" + strconv.Itoa(i)
	}
	available = append(available, instanceids)
	if len(wantedLabelNames) == 0 {
		// again sorting matters (which is provided by names())
		// and: global labels always come before local labels
		wantedLabelNames = append(l1.names(), l2.names()...)
		wantedLabelNames = append(wantedLabelNames, "instanceid")
	}
	ret := &labelCombinator{labelNames: append([]string{}, wantedLabelNames...)}
	for _, wanted := range wantedLabelNames {
		for _, l := range available {
			if wanted == l.Name {
				ret.addValues(l.Values)
				break
			}
		}
	}
	return ret
}

// labelCombinator
// holds the combinations of all metric label values of a certain metric
// is used to filter these combinations acc. to the provided labelSelectors
//
// it acts also per labelSelector to hold the selector-specific combinations
// this is to make it possible to filter on labelSelector level, what is used
// for the buckets (see README.md)
type labelCombinator struct {
	labelNames     []string
	combinedValues [][]string
}

// setLabelNames
// sets/overrides the labelNames
func (c *labelCombinator) setLabelNames(n []string) {
	c.labelNames = append([]string{}, n...)
}

// getCombinations
func (c *labelCombinator) getCombinations() [][]string {
	return c.combinedValues
}

// addValues
// is used to create the initial combinedValues list for each metric
func (c *labelCombinator) addValues(values []string) {
	newValues := [][]string{}
	if len(c.combinedValues) == 0 {
		// first run
		c.combinedValues = make([][]string, 1)
	}
	for _, v := range values {
		for _, oldVal := range c.combinedValues {
			tmp := append([]string{}, oldVal...)
			tmp = append(tmp, v)
			newValues = append(newValues, tmp)
		}
	}
	c.combinedValues = newValues
}

// addCombination
// is used to fill the labelSelector specific labelCombinator instances
func (c *labelCombinator) addCombination(combination []string) {
	c.combinedValues = append(c.combinedValues, combination)
}

// filter
// is used to filter the stored combinedValues for each passed labelSelector
// and add the labelSelector specific combinedValues
// func (c *labelCombinator) filter(s []metricUpdater) { => slices of interfaces don't seem to work
func (c *labelCombinator) filter(s specs) {
	sort.SliceStable(s, func(i, j int) bool { return len(s[i].getLabelSelector()) > len(s[j].getLabelSelector()) })
	for _, st := range s {
		st.setLabelNames(c.labelNames)
	}
O:
	for _, v := range c.combinedValues {
		// var st metricUpdater
		for _, st := range s {
			if c.doesMatch(v, st.getLabelSelector()) {
				st.addCombination(v)
				continue O
			}
		}
	}
}

// doesMatch
// helper function to see if a combinedValue matches a given labelSelector
func (c *labelCombinator) doesMatch(comb []string, ls labelSelector) bool {
	for k, v := range ls {
		i := 0
		n := ""
		for i, n = range c.labelNames {
			if n == k {
				break
			}
		}
		// assuming the labelNames have been validated properly, so not checking out-of-range errors
		if v != comb[i] {
			return false
		}
	}
	return true
}

type metric struct {
	Name          string         `yaml:"name"`
	Description   string         `yaml:"description"`
	MetricType    string         `yaml:"type"`
	Replicas      uint           `yaml:"replicas"`       // defaults to 1
	Expectation   float64        `yaml:"expectation"`    // the expected average value; defaults to 0
	Variation     uint           `yaml:"variation"`      // 0-100; defaults to 0
	Frequency     uint           `yaml:"frequency"`      // defaults to 1
	AppliedLabels appliedLabels  `yaml:"applied_labels"` // optional; if it is missing then the metric vector will be created for all defined labels
	Labels        labels         `yaml:"labels"`         // optional; labels applied in addition to the global labels
	Specs         specs          `yaml:"specs"`
	Buckets       buckets        `yaml:"buckets"`
	metricVectors []metricVector // a list because the various buckets supported by the Histograms
	// *labelCombinator                // runtime metric label combinations to be updated -> this is the default list when no spec exists or does match
}

func (m *metric) validate(l labels) error {
	if m.Name == "" {
		return errors.New("found metric without a 'name'")
	} else if m.Description == "" {
		return errors.New(fmt.Sprintf("metric '%s' without a 'description'", m.Name))
	} else if ok, _ := arrayInArray([]string{m.MetricType}, metricTypes); !ok {
		return errors.New(fmt.Sprintf("unknown metrictype '%s' in metric '%s', supported types are '%v'", m.MetricType, m.Name, metricTypes))
	} else if m.Variation > 100 {
		return errors.New(fmt.Sprintf("variation '%d' too big in metric '%s', values must be <=100", m.Variation, m.Name))
	} else if err := m.AppliedLabels.validate(l); err != nil {
		return errors.New(fmt.Sprintf("appliedLabels error in metric '%s': %s", m.Name, err))
	} else if err := m.Specs.validate(l, m.Labels, m.AppliedLabels); err != nil {
		return errors.New(fmt.Sprintf("specs error in metric '%s': %s", m.Name, err))
	} else if err := m.Buckets.validate(l, m.Labels, m.AppliedLabels); err != nil {
		return errors.New(fmt.Sprintf("buckets error in metric '%s': %s", m.Name, err))
	} else if m.MetricType == "histogram" && len(m.Buckets) == 0 {
		return errors.New(fmt.Sprintf("buckets are mandatory for histograms, missing in metric '%s'", m.Name))
	} else if m.MetricType != "histogram" && len(m.Buckets) > 0 {
		return errors.New(fmt.Sprintf("buckets are not compatible with metric type '%s' (only for histograms) in metric '%s'", m.MetricType, m.Name))
	}
	if m.Frequency == 0 {
		m.Frequency = 1 // maybe 0 can be used to simulate missing metrics, something for later...
	}
	if m.Replicas == 0 {
		m.Replicas = 1
	}
	return nil
}

// isEqualTo
// structs containing []string cannot be compared using ==
func (m1 *metric) isEqualTo(m2 *metric) bool {
	return m1.Name == m2.Name &&
		m1.Description == m2.Description &&
		m1.MetricType == m2.MetricType &&
		m1.Expectation == m2.Expectation &&
		m1.Variation == m2.Variation &&
		strings.Join(m1.AppliedLabels, ",") == strings.Join(m2.AppliedLabels, ",") &&
		m1.Labels.areEqualTo(m2.Labels) &&
		m1.Specs.areEqualTo(m2.Specs) &&
		m1.Buckets.areEqualTo(m2.Buckets)
}

// wantedLabelNames
// returns a 'sorted' list of all labels used by a metric
// the global labels are matched against the metric's appliedLabels (and filtered accordingly)
// the local labels are then appended
// so sorted in this case means the global labels are sorted and the local labels are sorted
// and the returned list is always in the same order
func (m *metric) wantedLabelNames(l labels) []string {
	ret := l.names()
	if len(m.AppliedLabels) > 0 {
		tmp := []string{}
		// loop through labels to keep order
		for _, label := range ret {
			applied := false
			for _, al := range m.AppliedLabels {
				if label == al {
					applied = true
					break
				}
			}
			if applied {
				tmp = append(tmp, label)
			}
		}
		ret = tmp
	}
	ret = append(ret, m.Labels.names()...)
	ret = append(ret, "instanceid")
	return ret
}

// merge
// just calls the vector's merge
func (m1 *metric) merge(m2 *metric, l labels) {
	for _, vec := range m1.metricVectors {
		vec.merge(m1, m2, l)
	}
}

// getLabelSelector
// returns an empty ls, assuming that the metrics act as a default for all label combinations
// that are not covered by the specs (or in case no specs are provided at all)
// func (m *metric) getLabelSelector() labelSelector {
// 	return make(labelSelector)
// }

type metrics []*metric

func (m metrics) validate(l labels) error {
	for _, metric := range m {
		if err := metric.validate(l); err != nil {
			return err
		}
	}
	return nil
}

// findMetric
// find a metric in a metric array
func (m metrics) findMetric(m2 *metric, l labels) *metric {
	for _, metric := range m {
		if metric.Name == m2.Name &&
			metric.MetricType == m2.MetricType &&
			strings.Join(metric.wantedLabelNames(l), ",") == strings.Join(m2.wantedLabelNames(l), ",") {
			return metric
		}
	}
	return nil
}

type appliedLabels []string

func (a appliedLabels) validate(l labels) error {
	if len(a) > 0 {
		if ok, missing := arrayInArray(a, l.names()); !ok {
			return errors.New(fmt.Sprintf("invalid key(s): '%v'; supported labels: '%v'", missing, l.names()))
		}
	}
	return nil
}

// label_selectors are applied to both global labels and local labels
type labelSelector map[string]string

func (l labelSelector) names() []string {
	names := []string{}
	for k, _ := range l {
		names = append(names, k)
	}
	sort.Strings(names)
	return names
}

// copy
// returns a copy of the labelSelector
// used to initialize the buckets
func (l labelSelector) copy() labelSelector {
	newLs := make(labelSelector)
	for k, v := range l {
		newLs[k] = v
	}
	return newLs
}

// global labels, local labels, applied labels
// a labelSelector is valid if all its labels are listed in global or local and in applied labels unless empty
// TODO: also check the values
func (l labelSelector) validate(gl, ll labels, al []string) error {
	gnames := gl.names()
	lnames := ll.names()
	allLabels := make([]string, len(gnames)+len(lnames))
	copy(allLabels, gnames)
	copy(allLabels[len(gnames):], lnames)
	selectedLabels := l.names()
	if ok, missing := arrayInArray(selectedLabels, allLabels); !ok {
		return errors.New(fmt.Sprintf("invalid key(s) in labelSelector: '%v', supported keys: '%v'", missing, allLabels))
	} else if len(al) != 0 {
		if ok, missing := arrayInArray(selectedLabels, al); !ok {
			return errors.New(fmt.Sprintf("key(s) in labelSelector: '%v' not contained appliedLabels: '%v'", missing, al))
		}
	}
	return nil
}

func (l1 labelSelector) isEqualTo(l2 labelSelector) bool {
	if len(l1) != len(l2) {
		return false
	}
	for k, v := range l1 {
		if v != l2[k] {
			return false
		}
	}
	return true
}

// interfaces cannot be passed as slices it seems
// and anyhow this was just needed in the first attempt when metric did
// not add a default spec but act as its own default spec, but since this
// has changed all is implemented by the specs
// type metricUpdater interface {
// 	addCombination(combination []string)
// 	getLabelSelector() labelSelector
// }

// spec
// A spec is applied to all instances of a MetricVector or to a set of those
// instances, acc. to the labelSelector.
// A spec can contain several fractions which are applied then to the instances
// of the MetricVector acc. to the fraction value, but in the specified order.
// The Expectation, Variation and Frequency values on spec level are used as
// defaults in case the fractions do not cover 100% of the metricVector's
// instances or if no fractions are provided at all.
type spec struct {
	Expectation   float64       `yaml:"expectation"` // the expected average value; defaults to 0
	Variation     uint          `yaml:"variation"`   // 0-100; defaults to 0
	Frequency     uint          `yaml:"frequency"`   // defaults to 1
	LabelSelector labelSelector `yaml:"label_selector"`
	Fractions     fractions     `yaml:"fractions"`
	*labelCombinator
}

func (s1 *spec) isEqualTo(s2 *spec) bool {
	return s1.Expectation == s2.Expectation &&
		s1.Variation == s2.Variation &&
		s1.Frequency == s2.Frequency &&
		s1.LabelSelector.isEqualTo(s2.LabelSelector)
}

func (s *spec) getLabelSelector() labelSelector {
	return s.LabelSelector
}

// copy
// returns a copy of a spec
// used to initialize the buckets
func (s *spec) copy() *spec {
	return &spec{
		Expectation:     s.Expectation,
		Variation:       s.Variation,
		Frequency:       s.Frequency,
		LabelSelector:   s.LabelSelector.copy(),
		Fractions:       s.Fractions, // should be safe to copy by ref, read-only
		labelCombinator: &labelCombinator{labelNames: append([]string{}, s.labelCombinator.labelNames...)},
	}
}

// refreshValues
// calculates a set of values for one specific metricVector's instance
// replicas and current are used in case fractions exist
// in order to select the proper fraction's values
// remember: we have one label per combination, so current does not represent
// the current replica but the current label combination; so this combination
// needs to be translated into the current replica first
func (s *spec) refreshValues(replicas, current, combinations uint) []float64 {
	base := float64(s.Expectation)
	variation := float64(s.Variation)
	frequency := s.Frequency
	labelsPerReplica := float64(combinations) / float64(replicas)
	if fraction := s.Fractions.getFraction(replicas, uint(math.Ceil(float64(current)/labelsPerReplica))); fraction != nil {
		base = float64(fraction.Expectation)
		variation = float64(fraction.Variation)
		frequency = fraction.Frequency
	}
	ret := make([]float64, 0, frequency)
	for i := uint(0); i < frequency; i++ {
		v := rand.Float64() * variation * 2
		ret = append(ret, base*(1-variation/100+v/100))
	}
	return ret
}

type specs []*spec

func (s specs) validate(gl, ll labels, al []string) error {
	for i, c := range s {
		if c.Variation > 100 {
			return errors.New(fmt.Sprintf("variation '%d' too big in spec '%d', values must be <=100", c.Variation, i))
		} else if c.Frequency == 0 {
			return errors.New(fmt.Sprintf("frequency must be >0 in spec '%d'", i))
		} else if err := c.LabelSelector.validate(gl, ll, al); err != nil {
			return errors.New(fmt.Sprintf("labelSelector error in spec '%d': %s", i, err.Error()))
		} else if err := c.Fractions.validate(); err != nil {
			return errors.New(fmt.Sprintf("fraction error in spec '%d': %s", i, err.Error()))
		}
		c.labelCombinator = new(labelCombinator)
	}
	return nil
}

func (s1 specs) areEqualTo(s2 specs) bool {
	if len(s1) != len(s2) {
		return false
	}
	for i, spec := range s1 {
		if !spec.isEqualTo(s2[i]) {
			return false
		}
	}
	return true
}

// fraction
// A fraction is used to apply different metric values to the same metricVector
// acc. to the Fraction value.
// The values are applied to the metricVector's instances in the same order
// as the fractions are provided. If the sum of all fractions is less than 100
// the values defined on the spec object are used to the remaining metricVector's
// instances.
// If the sum of all fractions of a spec exceed 100 the fractions > 100 are ignored.
type fraction struct {
	Expectation float64 `yaml:"expectation"` // the expected average value; defaults to 0
	Variation   uint    `yaml:"variation"`   // 0-100; defaults to 0
	Frequency   uint    `yaml:"frequency"`   // defaults to 1
	Fraction    uint    `yaml:"fraction"`    // 0-100; defaults to 0
}

func (f *fraction) validate() error {
	if f.Variation > 100 {
		return errors.New(fmt.Sprintf("variation '%d' too big, values must be <=100", f.Variation))
	} else if f.Frequency == 0 {
		return errors.New("frequency must be >0")
	} else if f.Fraction > 100 {
		return errors.New(fmt.Sprintf("fraction '%d' too big, values must be <=100", f.Fraction))
	}
	return nil
}

type fractions []*fraction

func (f fractions) validate() error {
	for i, c := range f {
		if err := c.validate(); err != nil {
			return errors.New(fmt.Sprintf("validation error in fraction %d: %s", i, err.Error()))
		}
	}
	return nil
}

func (f fractions) getFraction(replicas, current uint) *fraction {
	last := uint(0)
	pct := uint(float64(current) / float64(replicas) * 100)
	for _, c := range f {
		if c.Fraction+last >= pct {
			return c
		}
		last += c.Fraction
	}
	return nil
}

type bucket struct {
	Start         float64       `yaml:"start"`
	Width         float64       `yaml:"width"`
	Count         uint          `yaml:"count"`
	LabelSelector labelSelector `yaml:"label_selector"`
	specs
}

func (b *bucket) validate(gl, ll labels, al []string) error {
	if b.Width <= 0 {
		return errors.New("width must be >0")
	} else if b.Count == 0 {
		return errors.New("count must be >0")
	} else if err := b.LabelSelector.validate(gl, ll, al); err != nil {
		return errors.New(fmt.Sprintf("labelSelector error: %s", err.Error()))
	} else {
		return nil
	}
}

// isEqualTo
// the labelSelector is not used (yet) for the comparison
// because it should be ok to change it, new values have no histories
// and old values have no updates anymore, which is no different from
// prometheus point of view than recreating the buckets
func (b1 *bucket) isEqualTo(b2 *bucket) bool {
	return b1.Start == b2.Start &&
		b1.Width == b2.Width &&
		b1.Count == b2.Count &&
		b1.subsystem() == b2.subsystem() // &&
	// b1.LabelSelector.isEqualTo(b2.LabelSelector)
}

// subsystem
// to uniquely identify a histogram
func (b *bucket) subsystem() string {
	items := []string{}
	ret := ""
	// using names() instead of looping over the map simply, to keep sorted order
	for _, k := range b.LabelSelector.names() {
		items = append(items, k+":"+b.LabelSelector[k])
	}
	if len(items) > 0 {
		ret = strings.Join(items, "_")
	} else {
		ret = "default"
	}
	return ret
}

// func (b *bucket) getLabelSelector() labelSelector {
// 	return b.LabelSelector
// }

type buckets []*bucket

func (b buckets) validate(gl, ll labels, al []string) error {
	for i, bucket := range b {
		if err := bucket.validate(gl, ll, al); err != nil {
			return errors.New(fmt.Sprintf("validation error in bucket '%d': %s", i, err.Error()))
		}
	}
	return nil
}

func (b1 buckets) areEqualTo(b2 buckets) bool {
	if len(b1) != len(b2) {
		return false
	}
	for i, bucket := range b1 {
		if !bucket.isEqualTo(b2[i]) {
			return false
		}
	}
	return true
}

// find
// compares the buckets by subsystem name
// if a match is found it is returned
// otherwise nil is returned
func (b buckets) find(other *bucket) *bucket {
	for _, bucket := range b {
		if bucket.isEqualTo(other) {
			return bucket
		}
	}
	return nil
}

// createSpecs
// loops through the buckets and attaches each bucket a list of specs
// based on the specs that are attached to the containing metric
// this is (more or less) on purpose so that in the config the specs are independent
// from the buckets' labelSelectors which is more flexible than moving the specs
// into the buckets
func (b buckets) createSpecs(s specs) {
	for _, bucket := range b {
		for _, spec := range s {
			scopy := spec.copy()
			scopy.LabelSelector = bucket.LabelSelector.copy()
			spec.filter(specs{scopy})
			if len(scopy.getCombinations()) > 0 {
				bucket.specs = append(bucket.specs, scopy)
			}
		}
	}
}

func arrayInArray(t []string, a []string) (bool, []string) {
	missing := []string{}
	if len(t) == 0 {
		return true, nil
	}
OA:
	for i := range t {
		for j := range a {
			if a[j] == t[i] {
				continue OA
			}
		}
		missing = append(missing, t[i])
	}
	if len(missing) == 0 {
		return true, nil
	} else {
		return false, missing
	}
}

type metricVector interface {
	refresh(s specs) // set new values
	collector() prometheus.Collector
	merge(m1, m2 *metric, l labels)
	dirty(d bool) bool
}

type metricBase struct {
	isDirty  bool // for the merge
	replicas uint // used to refreshValues
}

// dirty
// sets the isDirty value and returns the old value
func (m *metricBase) dirty(d bool) bool {
	ret := m.isDirty
	m.isDirty = d
	return ret
}

// merge
// generic merge method
// re-uses the prev. metricVector
// operates on metric base because the metricVector is not part of base
// it assumes that the metric name, type and wantedLabelNames are equal
// and that there is only one vector part of the slice
func (b *metricBase) merge(m1, m2 *metric, l labels) {
	m1.metricVectors = m2.metricVectors
	m2.metricVectors = nil
	_ = m1.metricVectors[0].dirty(true)
}

type metricGaugeVec struct {
	*metricBase
	*prometheus.GaugeVec
}

func (m *metricGaugeVec) refresh(s specs) {
	for _, spec := range s {
		labels := spec.getCombinations()
		for i, l := range labels {
			values := spec.refreshValues(m.replicas, uint(i+1), uint(len(labels)))
			if verbosecnt%verbosefrequence == 0 {
				verbose.Printf("Gauge %v: %v => %v", l, spec, values)
			}
			for _, v := range values {
				m.WithLabelValues(l...).Set(float64(v))
			}
		}
	}
}

func (m *metricGaugeVec) collector() prometheus.Collector {
	return m.GaugeVec
}

type metricCounterVec struct {
	*metricBase
	*prometheus.CounterVec
}

func (m *metricCounterVec) refresh(s specs) {
	for _, spec := range s {
		labels := spec.getCombinations()
		for i, l := range labels {
			values := spec.refreshValues(m.replicas, uint(i+1), uint(len(labels)))
			if verbosecnt%verbosefrequence == 0 {
				verbose.Printf("Counter %v: %v => %v", l, spec, values)
			}
			for _, v := range values {
				m.WithLabelValues(l...).Add(float64(v))
			}
		}
	}
}

func (m *metricCounterVec) collector() prometheus.Collector {
	return m.CounterVec
}

type metricHistogramVec struct {
	*metricBase
	*bucket // to compare the vector
	*prometheus.HistogramVec
}

func (m *metricHistogramVec) refresh(s specs) {
	// ignoring the default specs, using the bucket specific ones instead
	for _, s := range m.bucket.specs {
		labels := s.getCombinations()
		for i, l := range labels {
			values := s.refreshValues(m.replicas, uint(i+1), uint(len(labels)))
			if verbosecnt%verbosefrequence == 0 {
				verbose.Printf("Histogram %s/%v: %v => %v", m.bucket.subsystem(), l, s, values)
			}
			for _, v := range values {
				m.WithLabelValues(l...).Observe(float64(v))
			}
		}
	}
}

func (m *metricHistogramVec) collector() prometheus.Collector {
	return m.HistogramVec
}

// merge
// histogram specific merge method (overriding baseMetric's merge)
// searches in m2 for the same bucket (same subsystem string)
// if the bucket range are equal the vector is reused
// otherwise the new one is used
// not sure if one can define a func on a type representing
// an array of an interface type; if that's possible it would
// be clearly an improvement here
func (m *metricHistogramVec) merge(m1, m2 *metric, l labels) {
	oldBucket := m2.Buckets.find(m.bucket)
	if oldBucket != nil {
		for i, vec := range m2.metricVectors {
			histogramVec := vec.(*metricHistogramVec)
			if histogramVec.bucket.subsystem() == oldBucket.subsystem() {
				m.HistogramVec = histogramVec.HistogramVec
				_ = m.dirty(true)
				m2.metricVectors = append(m2.metricVectors[:i], m2.metricVectors[i+1:]...)
				break
			}
		}
	}
}

type metricSummaryVec struct {
	*metricBase
	*prometheus.SummaryVec
}

func (m *metricSummaryVec) refresh(s specs) {
	for _, spec := range s {
		labels := spec.getCombinations()
		for i, l := range labels {
			values := spec.refreshValues(m.replicas, uint(i+1), uint(len(labels)))
			if verbosecnt%verbosefrequence == 0 {
				verbose.Printf("Summary %v: %v => %v", l, spec, values)
			}
			for _, v := range values {
				m.WithLabelValues(l...).Observe(float64(v))
			}
		}
	}
}

func (m *metricSummaryVec) collector() prometheus.Collector {
	return m.SummaryVec
}

// configFile
// returns ENV[CONFIG] || "<cwd>/metrics.yaml"
func configFile() (string, error) {
	if f := os.Getenv("CONFIG"); f != "" {
		return f, nil
	} else {
		if cwd, err := os.Getwd(); err != nil {
			return "", err
		} else {
			return cwd + "/metrics.yaml", nil
		}
	}
}

// readConfigToString
// returns the yaml string stored in the config file
func readConfigToString(config string) (string, error) {
	file, err := os.Open(config)
	if err != nil {
		return "", err
	}
	defer file.Close()
	buf := new(bytes.Buffer)
	if _, err := io.Copy(buf, file); err != nil {
		return "", err
	}
	return buf.String(), nil
}

// parseYamlString
// parses the metrics into config variable
func parseYamlString(data string) (labels, metrics, error) {
	metricConfig := new(config)
	if err := yaml.UnmarshalStrict([]byte(data), metricConfig); err != nil {
		return nil, nil, err
	} else {
		return metricConfig.Labels, metricConfig.Metrics, nil
	}
}

func recordMetrics(myReg *prometheus.Registry, configFileName string) {
	active_labels := labels{}
	active_metrics := metrics{}
	for {
		if configChanged {
			active_labels, active_metrics = reloadConfig(active_metrics, configFileName, myReg)
			configChanged = false
		}
		refresh(active_labels, active_metrics)
		time.Sleep(1 * time.Second)
		verbosecnt += 1
	}
}

func refresh(l labels, m metrics) {
	for _, metric := range m {
		for _, mv := range metric.metricVectors {
			mv.refresh(metric.Specs)
		}
	}
}

// initMetrics
// for each defined metric:
// - creates the metric vector
// - creates the specs' labelSelectors
// - assigning the specs to the buckets (for histograms)
func initMetrics(l labels, m metrics) {
	for _, metric := range m {
		metriclabels := metric.wantedLabelNames(l)
		lc := l.combinations(metric.Labels, metriclabels, metric.Replicas)
		// initially this was the plan, to treat the metric as a spec instance because of its default spec values
		// however with the buckets' need to 'copy' the specs we needed a 'real' default spec
		// therefore we attach this 'default spec' now to the end of the specs list
		// lc.filter([]metricUpdater{concat(m, m.Specs)})
		// but doing this only if Expectation > 0 assuming that only then the default makes sense/is intended
		// or if no specs are defined in which case the default is all we have
		if len(metric.Specs) == 0 || metric.Expectation > 0 {
			metric.Specs = append(metric.Specs, &spec{
				Expectation:     metric.Expectation,
				Variation:       metric.Variation,
				Frequency:       metric.Frequency,
				LabelSelector:   make(labelSelector), // empty labelSelector because this is the default one
				labelCombinator: &labelCombinator{},
			})
		}
		lc.filter(metric.Specs)
		switch metric.MetricType {
		case "gauge":
			metric.metricVectors = []metricVector{&metricGaugeVec{&metricBase{replicas: metric.Replicas}, prometheus.NewGaugeVec(prometheus.GaugeOpts{
				Name: metric.Name,
				Help: metric.Description,
			}, metriclabels)}}
		case "counter":
			metric.metricVectors = []metricVector{&metricCounterVec{&metricBase{replicas: metric.Replicas}, prometheus.NewCounterVec(prometheus.CounterOpts{
				Name: metric.Name,
				Help: metric.Description,
			}, metriclabels)}}
		case "summary":
			metric.metricVectors = []metricVector{&metricSummaryVec{&metricBase{replicas: metric.Replicas}, prometheus.NewSummaryVec(prometheus.SummaryOpts{
				Name: metric.Name,
				Help: metric.Description,
			}, metriclabels)}}
		case "histogram":
			for _, bucket := range metric.Buckets {
				subsystem := bucket.subsystem()
				buckets := []float64{bucket.Start}
				for i := uint(1); i <= bucket.Count; i++ {
					buckets = append(buckets, bucket.Start+bucket.Width*float64(i))
				}
				metric.metricVectors = append(metric.metricVectors, &metricHistogramVec{&metricBase{replicas: metric.Replicas}, bucket, prometheus.NewHistogramVec(prometheus.HistogramOpts{
					Name:      metric.Name,
					Subsystem: subsystem,
					Help:      metric.Description,
					Buckets:   buckets,
				}, metriclabels)})
			}
			metric.Buckets.createSpecs(metric.Specs)
		}
	}
}

// mergeMetrics
// merges the old metrics into the new ones
// supposed to be called before registerMetrics and clearMetrics
func mergeMetrics(newM, oldM metrics, l labels) {
	for _, m := range newM {
		verbose.Printf("merging %s...", m.Name)
		if om := oldM.findMetric(m, l); om != nil {
			m.merge(om, l)
		}
	}
}

func clearMetrics(reg *prometheus.Registry, m metrics) {
	for _, metric := range m {
		verbose.Printf("clearing %s...", metric.Name)
		for i, vec := range metric.metricVectors {
			if ok := reg.Unregister(vec.collector()); !ok {
				verbose.Printf("vector %d was already unregistered it seems", i)
			}
		}
	}
}

func registerMetrics(reg *prometheus.Registry, m metrics) error {
	for _, metric := range m {
		verbose.Printf("registering vectors of metric %s", metric.Name)
		for i, vector := range metric.metricVectors {
			if dirty := vector.dirty(false); dirty {
				verbose.Printf("vector %d is registered already, no need register again", i)
				continue
			}
			if err := reg.Register(vector.collector()); err != nil {
				return errors.New(fmt.Sprintf("registry error for metric '%s' vector %d: %s", metric.Name, i, err.Error()))
			}
		}
	}
	return nil
}

func inotify(filename string) {
	buff := make([]byte, 64)
	inotefd, err := syscall.InotifyInit()
	if err != nil {
		fmt.Errorf("error initializing inotify: %s\n", err.Error())
		os.Exit(1)
	}
	_, err = syscall.InotifyAddWatch(inotefd, filename, syscall.IN_MODIFY)
	if err != nil {
		fmt.Errorf("error watching file %s: %s\n", filename, err.Error())
		os.Exit(1)
	}

	fmt.Printf("watching %s...\n", filename)

	for {
		// IMPROVE ME: the Read returns three times with every change it seems,
		// buff needs to be checked probably to distinguish the relevant calls
		n, err := syscall.Read(inotefd, buff)
		if err != nil {
			fmt.Errorf("error reading inotify event: %s\n", err.Error())
			os.Exit(1)
		}

		if n < 0 {
			fmt.Errorf("received empty inotify event")
			os.Exit(1)
		}

		verbose.Println("config file changed")
		configChanged = true
	}

}

func reloadConfig(active_metrics metrics, configFileName string, myReg *prometheus.Registry) (labels, metrics) {
	configString, cfgstringerr := readConfigToString(configFileName)
	if cfgstringerr != nil {
		fmt.Printf("configRead error: %s\n", cfgstringerr.Error())
		os.Exit(1)
	}
	_labels, _metrics, cfgyamlerr := parseYamlString(configString)
	if cfgyamlerr != nil {
		fmt.Printf("configParse error: %s\n", cfgyamlerr.Error())
		os.Exit(1)
	}

	if err := _labels.validate(); err != nil {
		fmt.Printf("labels validation error: %s\n", err)
		os.Exit(1)
	}
	if err := _metrics.validate(_labels); err != nil {
		fmt.Printf("metrics validation error: %s\n", err)
		os.Exit(1)
	}
	_labels.sortValues()

	initMetrics(_labels, _metrics)
	mergeMetrics(_metrics, active_metrics, _labels)
	clearMetrics(myReg, active_metrics)
	if err := registerMetrics(myReg, _metrics); err != nil {
		fmt.Printf("metrics registration error: %s", err.Error())
		os.Exit(1)
	}
	return _labels, _metrics
}

func main() {
	myReg := prometheus.NewRegistry()

	if cfgfile, cfgfileerr := configFile(); cfgfileerr != nil {
		fmt.Printf("configFilename error: %s\n", cfgfileerr.Error())
		os.Exit(1)
	} else {
		go inotify(cfgfile)
		go recordMetrics(myReg, cfgfile)
		timeout, _ := time.ParseDuration("1s")
		http.Handle("/metrics", promhttp.HandlerFor(myReg, promhttp.HandlerOpts{nil, 0, true, 1, timeout}))
		http.ListenAndServe(":2112", nil)
	}

}
