.PHONY: build test docker-build
build:
	GO_ENABLED=0 go build -o promdemo main.go
test:
	go test *.go -v
docker-build:
	docker build -t promdemo:latest .
