ARG BASE=golang:1.11
FROM $BASE as build
WORKDIR /src
COPY main.go main_test.go metrics.yaml vendor/ ./
ENV GOPATH /
RUN go test *.go -v
RUN CGO_ENABLED=0 go build -o promdemo main.go
FROM scratch
WORKDIR /app
EXPOSE 2112/tcp
COPY --from=build /src/promdemo /src/metrics.yaml ./
USER 10000:10000
ENTRYPOINT ["/app/promdemo"]