# Promdemo
A tool to create Prometheus metrics based on the provided input, for demo/test purposes.
# Config
The configuration consists of a list of labels and a list of metrics.  
See [metrics.yaml](./metrics.yaml) for a sample config.
## <a name="labels"></a>labels
Defines the default labels (names and values) that are applied to all metrics.  
The resulting key/value pairs is a cross sum of all possible combinations.  
It can be incfluenced on a per-metric base using appliedLabels (see below).  
The label `instanceid` is reserved (for the replicas count) and should not be used.
## metrics
Defines the metrics that are created.  
Supported types:
* Counter
* Gauge
* Summary
* Histogram
### <a name="mvalues"></a>metric values
These three attributes are relevant for the values that are applied to the metrics:
* expectation (float64)
  The base for the value of the metric.
* variation (uint; 0<=x<=100)
  Percentage how much the actual value can vary from the expectation (0 = no variation; 100 = max. variation).  
  The percent value is a max., the actual variation is a random value between -percentage and percentage.  
  For ex. the expectation 10 and variation 20 produces actual values between 8.0 and 12.0.
* frequency (uint)
  How often the value should be applied to the metric per second.  
  (For each value the variation is applied again, the same value is not applied twice.)
### applied_labels
A string array that specifies for which [labels](#labels) the metric should be exposed.  
Check the `cache_uptodate` metric in the [examples](./metrics.yaml).
### labels
An optional element that is used to add more labels to a specific metric.  
Check the `http_request_response_seconds` metric in the [examples](./metrics.yaml)  
where the HTTP verbs are added as additional labels.  
The label `instanceid` is reserved (for the replicas count) and should not be used.
### <a name="specs"></a>specs
Use the specs element to vary the metrics' values depending on the label it is applied for.  
The `specs` element is an array of `spec` elements.  
Each `spec` element contains the [three relevant attributes](#mvalues) `expectation`, `variation` and `frequency`  
and a mandatory [label_selector](#labelselector) element.
#### <a name="labelselector"></a>label_selector
The `label_selector` is used to restrict a `spec` element to a certain subset of label combinations.  
For ex. if you produce metrics for a 'prd' and a 'tst' environment, you can use the `label_selector` to  
produce different values (or ranges of values) for each of them.  
Check the `bookings_total` metric in the [examples](./metrics.yaml).
### <a name="fractions"></a>fractions
An optional element used to divide the matching replicas (behind the label combinations) and assign different values to each group.  
The intention behind this is to demo for ex. the effect of n% of the metrics showing 'misbehaving' values.  
The `fractions` element is an array of `fraction` elements.  
Each `fraction` element contains the [three relevant attributes](#mvalues) `expectation`, `variation` and `frequency` plus one additional attribute called `fraction` which contains the percentage (as uint 0<x<100) of replicas the fraction will be applied to.  
The fraction values are additive to the values of the fraction elements earlier in the list.__
The values defined on `spec` level are used as defaults in case the fractions do not sum up to 100% ()of the replicas).
### other attributes
* name - the metric's name
* description - the metric's description
* replicas - the number of replicas of a specific metric; defaults to 1
* type - one of _counter_, _gauge_, _summary_, _histogram_
### buckets (Histogram only)
Prometheus Histograms store the metric values in buckets to have a more precise measurement than just the average values that a Summary provides.  
Check the (official Histogram page)[https://prometheus.io/docs/practices/histograms/] for a recap.  
A `buckets` element is a list of `bucket` elements which contain the following attributes:
* start (float64) - the start value of the first bucket
* width (float64) - the width of each bucket
* count (uint) - the number of buckets
* (label_selector)[#labelselector] (optional) - to define different buckets in the same way as it works for (the specs)[#specs]
A single `bucket`` element represents one Histogram with the buckets configured according to the `bucket's` start/width/count values.
## Source
The environment variable `CONFIG` can be used to point the script to the config file it should use.  
Per default the script looks for a `metrics.yaml` file in the current working directory.  
If the configuration changes while the script is running it reads the config again and updates its specs  
and deletes metrics if they disappeared and creates new metrics if some were added.
# Installation
The latest version is available at docker.io/manzthereader/promdemo:latest.
## Docker
`docker run -d --rm manthereader/promdemo`
## Minikube/Helm
The Helm chart contains subcharts including Prometheus and Grafana.
* Install the helm-tiller plugin `helm plugin install https://github.com/rimusz/helm-tiller`.
* Run `helm tiller run -- helm intall --name promdemo --namespace promdemo .` from inside the helm directory. Check the [README](./helm/README.md) for more info.
* Both Prometheus and Grafana are exposed using nodePorts; Prometheus listens at 30090, Grafana at 32000.
* The Grafana credentials are admin/grafana. There is a sample dashboard 'promdemo' with basic example graphs for the default metrics that come with promdemo.
### Namespaces
It would be nice to be able to install all charts in dedicated namespaces to make it more look like a 'real' environment.  
This is not supported yet though in Helm: https://github.com/helm/helm/issues/5358.
# Debugging
First you need to expose the port to your local box; you can use one of the examples below.  
The you can run `curl localhost:2112/metrics` to see the exposes metrics.  
The script will also write the metric values to stdout every 30 seconds. Alternatively you can connect to the script's logs to see those values.
## Docker
`docker run -d --rm -p 2112:2112 manthereader/promdemo`
## Kubernetes
To debug the metrics you can create a port-forwarding for the pod running the demo:  
`kubectl port-forward <podname> -n promdemo 2112` connects to your pod and forwards traffic from your local box to the pod.
# Internals
## labelCombinator
A labelCombinator is used to hold all possible combinations of labels for each metric.  
A metric holds the default specs for its vectors.  
But a metric can also contain a list of spec instances which apply certain metric values to  
a subset of label combinations. This is what the _filter_ function is used for.  
The buckets used in histograms contain their own labelSelector instances. In order to match the  
spec instances to the bucket instances the spec instances contain their own labelCombinators.  
In such a way you can use the labelCombinators to filter the label combinations as much as needed.
## change in replicas
Replicas are represented by the label `instanceid`.  
Increasing the replicas will produce more metrics by adding more label combinations;  
decreasing it will lead to metrics missing values.
## comparing the metrics (in case of config changes)
* metrics are identified by name (if a metric name changes, it means delete the old and create new ones)
* wantedLabelNames need to be stable, because they are part of the metrics' definition,
  a change of labelNames means re-creating the metrics
* metric type change leads to re-creation
* buckets
  * labelSelector is part of the name (subsystem), change in there causes re-creation
  * bucket range changes lead to re-creation
* spec changes go without impact
  only in case of buckets, the bucket specs need to be overridden with the new ones
So during re-reading the config we just compare the above crucial parts. And simply re-assign the specs (plus re-create the default)  
and re-assign the buckets' specs. And replace only the vectors that have changed acc. to the above definition.
