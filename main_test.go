package main

import (
	"github.com/prometheus/client_golang/prometheus"
	dto "github.com/prometheus/client_model/go"
	"os"
	"sort"
	"strings"
	"testing"
	"time"
)

func TestConfigFile(t *testing.T) {
	envmetrics := "/etc/metrics.yaml"
	os.Setenv("CONFIG", envmetrics)
	if byenv, err := configFile(); err != nil {
		t.Errorf("byenv error: %s", err.Error())
	} else if byenv != envmetrics {
		t.Errorf("byenv path returned '%s' does not match expected path '%s'", byenv, envmetrics)
	}
	os.Unsetenv("CONFIG")
	cwd, cwderr := os.Getwd()
	if cwderr != nil {
		t.Errorf("error getting current working directory: %s", cwderr.Error())
	}
	if defaultenv, err := configFile(); err != nil {
		t.Errorf("defaultenv error: %s", err.Error())
	} else if defaultenv != cwd+"/metrics.yaml" {
		t.Errorf("defaultenv path returned '%s' does not match expected path '%s'", defaultenv, cwd+"/metrics.yaml")
	}
}

func TestParseYamlString(t *testing.T) {
	yamlstring := `---
labels:
  - name: customer
    values:
      - AirlineA
      - AirlineB
  - name: environment
    values:
      - PRD
      - TST
metrics:
  - name: cache_uptodate
    description: a 'boolean' value to demonstate ok/not ok statuses
    type: gauge
    expectation: 0
    variation: 0
    frequency: 1
    applied_labels:
      - environment
`
	badstring := `---
labels:
  - name: customer
    values:
      - AirlineA
      - AirlineB
  - name: environment
    values:
      - PRD
      - TST
metrics:
  - nameX: cache_uptodate
    description: a 'boolean' value to demonstate ok/not ok statuses
    type: gauge
    expectation: 0
    variation: 0
    frequency: 1
    labels:
      - environment
`
	label1 := &label{"customer", []string{"AirlineA", "AirlineB"}}
	label2 := &label{"environment", []string{"PRD", "TST"}}
	metric1 := &metric{"cache_uptodate",
		"a 'boolean' value to demonstrate ok/not ok statuses",
		"gauge",
		1,
		0,
		0,
		1,
		[]string{"environment"},
		nil,
		nil,
		nil,
		nil,
	}
	if labels, metrics, err := parseYamlString(yamlstring); err != nil {
		t.Errorf("error parsing yaml string: %s", err.Error())
	} else {
		sort.SliceStable(labels, func(i, j int) bool { return labels[i].Name < labels[j].Name })
		sort.SliceStable(metrics, func(i, j int) bool { return metrics[i].Name < metrics[j].Name })
		if len(labels) != 2 {
			t.Errorf("expected two label definitions but found %d", len(labels))
		} else if !labels[0].isEqualTo(label1) {
			t.Errorf("expected first label to be '%v' but got '%v'", label1, labels[0])
		} else if !labels[1].isEqualTo(label2) {
			t.Errorf("expected second label to be '%v' but got '%v'", label2, labels[1])
		}
		if len(metrics) != 1 {
			t.Errorf("expected one metric definitions but found %d", len(metrics))
		} else if metrics[0].isEqualTo(metric1) {
			t.Errorf("expected metric to be '%v' but got '%v'", metric1, metrics[0])
		}
	}
	if _, _, err := parseYamlString(badstring); err == nil {
		t.Errorf("expected error parsing bad yaml string but got none")
	}
}

func TestLabelValidation(t *testing.T) {
	goodLabel := &label{"env", []string{"PRD"}}
	nonameLabel := &label{Values: []string{"PRD"}}
	novalueLabel := &label{Name: "env"}
	if err := goodLabel.validate(); err != nil {
		t.Errorf("unexpected error %s on goodLabel", err.Error())
	}
	if err := nonameLabel.validate(); err == nil {
		t.Errorf("expected error for nonameLabel but got nothing")
	} else if err.Error() != "found label without a 'name'" {
		t.Errorf("expected error 'found label without a 'name'' but got '%s' instead", err.Error())
	}
	if err := novalueLabel.validate(); err == nil {
		t.Errorf("expected error for novalueLabel but got nothing")
	} else if err.Error() != "label 'env' without values" {
		t.Errorf("expected error 'label 'env' without values' but got '%s' instead", err.Error())
	}
}

func TestFractionValidation(t *testing.T) {
	goodF := &fraction{10, 10, 10, 10}
	badVariation := &fraction{10, 1000, 10, 10}
	badFraction := &fraction{10, 10, 10, 1000}
	if err := goodF.validate(); err != nil {
		t.Errorf("Expected goodF to validate but got an error instead: %s", err.Error())
	}
	if err := badVariation.validate(); err == nil {
		t.Errorf("Expected badVariation to return an error but the validation succeeded instead")
	} else if err.Error() != "variation '1000' too big, values must be <=100" {
		t.Errorf("Expected badVariation to return the error 'variation '1000' too big, values must be <=100' but got this instead: '%s'", err.Error())
	}
	if err := badFraction.validate(); err == nil {
		t.Errorf("Expected badFraction to return an error but the validation succeeded instead")
	} else if err.Error() != "fraction '1000' too big, values must be <=100" {
		t.Errorf("Expected badFraction to return the error 'fraction '1000' too big, values must be <=100' but got this instead: '%s'", err.Error())
	}
	Fs := fractions{goodF, badVariation}
	if err := Fs.validate(); err == nil {
		t.Errorf("Expected fractions to return an error but the validation succeeded instead")
	} else if err.Error() != "validation error in fraction 1: variation '1000' too big, values must be <=100" {
		t.Errorf("Expected badFraction to return the error 'validation error in fraction 1: variation '1000' too big, values must be <=100' but got this instead: '%s'", err.Error())
	}
}

func TestGetFraction(t *testing.T) {
	f1 := &fraction{10, 10, 10, 10}
	f2 := &fraction{20, 20, 20, 20}
	Fs := fractions{f1, f2}
	if g1 := Fs.getFraction(200, 15); g1 != f1 {
		t.Errorf("expected g1 to match f1 but got this instead: %v", g1)
	}
	if g2 := Fs.getFraction(200, 55); g2 != f2 {
		t.Errorf("expected g2 to match f2 but got this instead: %v", g2)
	}
	if g3 := Fs.getFraction(200, 65); g3 != nil {
		t.Errorf("expected g3 to be nil but got this instead: %v", g3)
	}
}

func TestLabelSelectorName(t *testing.T) {
	label_selector := make(labelSelector)
	label_selector["foo"] = "bar"
	label_selector["baz"] = "foobar"
	names := label_selector.names()
	if strings.Join(names, ",") != "baz,foo" {
		t.Errorf("expected names to be 'baz,foo' but got '%s'", strings.Join(names, ","))
	}
}

func TestLabelSelectorEqual(t *testing.T) {
	label_selector := make(labelSelector)
	label_selector["foo"] = "bar"
	label_selector["baz"] = "foobar"
	label_selectorSame := make(labelSelector)
	label_selectorSame["foo"] = "bar"
	label_selectorSame["baz"] = "foobar"
	label_selectorLess := make(labelSelector)
	label_selectorLess["foo"] = "bar"
	label_selectorDiff := make(labelSelector)
	label_selectorDiff["foo"] = "bar"
	label_selectorDiff["baz"] = "barfoo"
	if !label_selector.isEqualTo(label_selectorSame) {
		t.Errorf("expected label_selector to be equal to label_selectorSame but it ain't")
	}
	if label_selector.isEqualTo(label_selectorLess) {
		t.Errorf("expected label_selector not to be equal to label_selectorLess but it is")
	}
	if label_selector.isEqualTo(label_selectorDiff) {
		t.Errorf("expected label_selector not to be equal to label_selectorSame but it is")
	}
}

func TestLabelSelectorValidation(t *testing.T) {
	globals := labels{
		&label{"environment", []string{"PRD", "TST"}},
		&label{"customer", []string{"customerA", "customerB"}},
	}
	locals := labels{
		&label{"http_method", []string{"get", "post", "put", "delete"}},
	}
	appliedLabelsAll := []string{}
	appliedLabelsEnv := []string{"environment"}
	label_selectorEnv := make(labelSelector)
	label_selectorEnv["environment"] = "PRD"
	label_selectorHttp := make(labelSelector)
	label_selectorHttp["http_method"] = "get"
	label_selectorBad := make(labelSelector)
	label_selectorBad["foo"] = "bar"
	if err := label_selectorEnv.validate(globals, locals, appliedLabelsEnv); err != nil {
		t.Errorf("selectorEnv unexpected error: %s", err.Error())
	}
	if err := label_selectorEnv.validate(globals, locals, appliedLabelsAll); err != nil {
		t.Errorf("selectorEnv(all) unexpected error: %s", err.Error())
	}
	if err := label_selectorHttp.validate(globals, locals, appliedLabelsEnv); err == nil {
		t.Errorf("selectorHttp should return an error but got none")
	} else if err.Error() != "key(s) in labelSelector: '[http_method]' not contained appliedLabels: '[environment]'" {
		t.Errorf("selectorHttp expected error: 'key(s) in labelSelector: '[http_method]' not contained appliedLabels: '[environment]'' but got '%s'", err.Error())
	}
	if err := label_selectorBad.validate(globals, locals, appliedLabelsAll); err == nil {
		t.Errorf("selectorBad should return an error but got none")
	} else if err.Error() != "invalid key(s) in labelSelector: '[foo]', supported keys: '[customer environment http_method]'" {
		t.Errorf("selectorBad expected error: 'invalid key(s) in labelSelector: '[foo]', supported keys: '[customer environment http_method]' but got '%s'", err.Error())
	}
}

func TestAppliedLabelsValidation(t *testing.T) {
	// applied labels only valid for global labels
	globals := labels{
		&label{"environment", []string{"PRD", "TST"}},
		&label{"customer", []string{"customerA", "customerB"}},
	}
	labelsgood := appliedLabels{"environment", "customer"}
	labelsbadunknown := appliedLabels{"foo"}
	labelsempty := appliedLabels{}
	if err := labelsgood.validate(globals); err != nil {
		t.Errorf("labelsgood unexpected error: %s", err.Error())
	}
	if err := labelsbadunknown.validate(globals); err == nil {
		t.Errorf("labelsbadunknown expected error but got none")
	} else if err.Error() != "invalid key(s): '[foo]'; supported labels: '[customer environment]'" {
		t.Errorf("labelsgood expected error: 'invalid key(s): '[foo]'; supported labels: '[customer environment]'' but got '%s'", err.Error())
	}
	if err := labelsempty.validate(globals); err != nil {
		t.Errorf("labelsempty unexpected error: %s", err.Error())
	}
}

func TestSpecValidation(t *testing.T) {
	globals := labels{
		&label{"environment", []string{"PRD", "TST"}},
		&label{"customer", []string{"customerA", "customerB"}},
	}
	locals := labels{}
	label_selector := make(labelSelector)
	label_selector["environment"] = "PRD"
	specgood := specs{&spec{1, 1, 1, label_selector, fractions{}, nil}, &spec{1, 1, 1, label_selector, fractions{}, nil}}
	specbadvariation := specs{&spec{1, 111, 1, label_selector, fractions{}, nil}}
	specbadfrequency := specs{&spec{1, 1, 0, label_selector, fractions{}, nil}}
	if err := specgood.validate(globals, locals, []string{}); err != nil {
		t.Errorf("specgood unexpected error: %s", err.Error())
	}
	if err := specbadvariation.validate(globals, locals, []string{}); err == nil {
		t.Errorf("specbadvariation expected error but got none")
	} else if err.Error() != "variation '111' too big in spec '0', values must be <=100" {
		t.Errorf("labelsgood expected error: 'variation '111' too big in spec '0', values must be <=100' but got '%s'", err.Error())
	}
	if err := specbadfrequency.validate(globals, locals, []string{}); err == nil {
		t.Errorf("specbadfrequency expected error but got none")
	} else if err.Error() != "frequency must be >0 in spec '0'" {
		t.Errorf("labelsgood expected error: 'frequency must be >0 in spec '0'' but got '%s'", err.Error())
	}
}

func TestSpecEquality(t *testing.T) {
	label_selector := make(labelSelector)
	label_selector["environment"] = "PRD"
	label_selector2 := make(labelSelector)
	label_selector2["environment"] = "TST"
	s1 := &spec{1, 1, 1, label_selector, fractions{}, nil}
	s2 := &spec{1, 1, 1, label_selector2, fractions{}, nil}
	s3 := &spec{2, 1, 1, label_selector, fractions{}, nil}
	if !s1.isEqualTo(s1) {
		t.Error("s1 should be equal to itself but is not")
	}
	if s1.isEqualTo(s2) {
		t.Error("s1 should be different from s2 but is equal")
	}
	if s1.isEqualTo(s3) {
		t.Error("s1 should be different from s3 but is equal")
	}
}

func TestBucketValidate(t *testing.T) {
	globals := labels{
		&label{"environment", []string{"PRD", "TST"}},
		&label{"customer", []string{"customerA", "customerB"}},
	}
	locals := labels{
		&label{"http_method", []string{"post", "get"}},
	}
	label_selector := make(labelSelector)
	label_selector["http_method"] = "post"
	bucketgood := &bucket{0, 1, 1, label_selector, nil}
	bucketBadwidth := &bucket{0, 0, 1, label_selector, nil}
	bucketBadcount := &bucket{0, 1, 0, label_selector, nil}
	if err := bucketgood.validate(globals, locals, []string{}); err != nil {
		t.Errorf("buketgood validation error: %s", err.Error())
	}
	if err := bucketBadwidth.validate(globals, locals, []string{}); err == nil {
		t.Errorf("bucketBadwidth validates okay but should have thrown an error")
	} else if err.Error() != "width must be >0" {
		t.Errorf("buketBadwidth expected error 'width must be >0' but got '%s' instead", err.Error())
	}
	if err := bucketBadcount.validate(globals, locals, []string{}); err == nil {
		t.Errorf("bucketBadcount validates okay but should have thrown an error")
	} else if err.Error() != "count must be >0" {
		t.Errorf("buketBadcount expected error 'count must be >0' but got '%s' instead", err.Error())
	}
}

func TestBucketEquality(t *testing.T) {
	label_selector := make(labelSelector)
	label_selector["environment"] = "PRD"
	label_selector2 := make(labelSelector)
	label_selector2["environment"] = "TST"
	b1 := &bucket{1, 1, 1, label_selector, nil}
	b2 := &bucket{1, 1, 1, label_selector2, nil}
	b3 := &bucket{2, 1, 1, label_selector, nil}
	if !b1.isEqualTo(b1) {
		t.Error("b1 should be equal to itself but is not")
	}
	if b1.isEqualTo(b2) {
		t.Error("b1 should be different from b2 but is equal")
	}
	if b1.isEqualTo(b3) {
		t.Error("b1 should be different from b3 but is equal")
	}
}

func TestArrayinArray(t *testing.T) {
	a1 := []string{"hello", "array"}
	a2 := []string{"array"}
	a3 := []string{"hello", "array", "again"}
	if ok, _ := arrayInArray(a1, a1); !ok {
		t.Errorf("a1 should be equal to itself but isn't")
	}
	if ok, _ := arrayInArray(a2, a1); !ok {
		t.Errorf("a2 should be contained in a2 but isn't")
	}
	if ok, missing := arrayInArray(a3, a1); ok {
		t.Errorf("a3 should not be contained in a2 but actually is")
	} else if len(missing) != 1 {
		t.Errorf("missing should return one missing element but have '%s'", strings.Join(missing, ","))
	} else if missing[0] != "again" {
		t.Errorf("missing should return 'again' but have '%s'", missing[0])
	}
}

func TestMetricValidation(t *testing.T) {
	globals := []*label{&label{"env", []string{"PRD"}}, &label{"peak", []string{"p1"}}}
	goodMetric := &metric{"cpu_usage", "cpu usage", "gauge", 1, 42, 95, 1, []string{"peak", "env"}, nil, nil, nil, nil}
	goodMetric2 := &metric{"cpu_usage", "cpu usage", "gauge", 1, 42, 95, 1, []string{}, nil, nil, nil, nil}
	nonameMetric := &metric{"", "cpu usage", "gauge", 1, 42, 95, 1, []string{"peak", "env"}, nil, nil, nil, nil}
	nodescMetric := &metric{"env", "", "gauge", 1, 42, 95, 1, []string{"peak", "env"}, nil, nil, nil, nil}
	badtypeMetric := &metric{"cpu_usage", "cpu usage", "unsupported", 1, 42, 95, 1, []string{"peak", "env"}, nil, nil, nil, nil}
	outofboundsMetric := &metric{"cpu_usage", "cpu usage", "gauge", 1, 42, 195, 1, []string{"peak", "env"}, nil, nil, nil, nil}
	badlabelMetric := &metric{"cpu_usage", "cpu usage", "gauge", 1, 42, 95, 1, []string{"peak", "customer"}, nil, nil, nil, nil}
	if err := goodMetric.validate(globals); err != nil {
		t.Errorf("goodMetric should validate but returns error: %s", err.Error())
	}
	if err := goodMetric2.validate(globals); err != nil {
		t.Errorf("goodMetric2 should validate but returns error: %s", err.Error())
	}
	if err := nonameMetric.validate(globals); err == nil {
		t.Errorf("nonameMetric should not validate but actually does")
	} else if err.Error() != "found metric without a 'name'" {
		t.Errorf("nonameMetric should return error 'found metric without a 'name'' but got '%s' instead", err.Error())
	}
	if err := nodescMetric.validate(globals); err == nil {
		t.Errorf("nodescMetric should not validate but actually does")
	} else if err.Error() != "metric 'env' without a 'description'" {
		t.Errorf("nodescMetric should return error 'metric 'env' without a 'description'' but got '%s' instead", err.Error())
	}
	if err := badtypeMetric.validate(globals); err == nil {
		t.Errorf("badtypeMetric should not validate but actually does")
	} else if err.Error() != "unknown metrictype 'unsupported' in metric 'cpu_usage', supported types are '[gauge counter summary histogram]'" {
		t.Errorf("badtypeMetric should return error 'unknown metrictype 'unsupported' in metric 'cpu_usage', supported types are '[gauge counter summary histogram]'' but got '%s' instead", err.Error())
	}
	if err := outofboundsMetric.validate(globals); err == nil {
		t.Errorf("outofboundsMetric should not validate but actually does")
	} else if err.Error() != "variation '195' too big in metric 'cpu_usage', values must be <=100" {
		t.Errorf("outofboundsMetric should return error 'variation '195' too big in metric 'cpu_usage', values must be <=100' but got '%s' instead", err.Error())
	}
	if err := badlabelMetric.validate(globals); err == nil {
		t.Errorf("badlabelMetric should not validate but actually does")
	} else if err.Error() != "appliedLabels error in metric 'cpu_usage': invalid key(s): '[customer]'; supported labels: '[env peak]'" {
		t.Errorf("badlabelMetric should return error 'appliedLabels error in metric 'cpu_usage': invalid key(s): '[customer]'; supported labels: '[env peak]'' but got '%s' instead", err.Error())
	}
}

func TestBaseMetricValues(t *testing.T) {
	s := &spec{100, 5, 10, nil, fractions{}, nil}
	v := s.refreshValues(1, 1, 1)
	if len(v) != 10 {
		t.Errorf("expected 10 values but got %d instead", len(v))
	}
	for i := 0; i < 10; i++ {
		if v[i] < 95 || v[i] > 105 {
			t.Errorf("all values should be 95 <= v <= 105; but value %d is at %f instead", i, v[i])
		}
	}
}

func TestMetricWantedLabelNames(t *testing.T) {
	m1 := &metric{"cpu_usage", "cpu usage", "gauge", 1, 42, 95, 1, []string{}, nil, nil, nil, nil}
	m2 := &metric{"cpu_usage", "cpu usage", "gauge", 1, 42, 95, 1, []string{"env"}, nil, nil, nil, nil}
	m3 := &metric{"cpu_usage", "cpu usage", "gauge", 1, 42, 95, 1, []string{"env"}, labels{&label{"method", []string{"post", "get"}}}, nil, nil, nil}
	globals := []*label{&label{"env", []string{"PRD"}}, &label{"peak", []string{"p1"}}}
	if names := m1.wantedLabelNames(globals); len(names) != 3 {
		t.Errorf("expected 3 label names from m1 '[env peak instanceid]' but got '%v' instead", names)
	} else if strings.Join(names, ",") != "env,peak,instanceid" {

		t.Errorf("expected label names '[env peak instanceid]' from m1 but got '%v' instead", names)
	}
	if names := m2.wantedLabelNames(globals); len(names) != 2 {
		t.Errorf("expected 2 label name from m2 '[env instanceid]' but got '%v' instead", names)
	} else if strings.Join(names, ",") != "env,instanceid" {
		t.Errorf("expected label name '[env instanceid]' from m2 but got '%v' instead", names)
	}
	if names := m3.wantedLabelNames(globals); len(names) != 3 {
		t.Errorf("expected 3 label names from m3 '[env method instanceid]' but got '%v' instead", names)
	} else if strings.Join(names, ",") != "env,method,instanceid" {
		t.Errorf("expected label names '[env method instanceid]' from m3 but got '%v' instead", names)
	}
}

func TestBucketSubsystem(t *testing.T) {
	ls1 := make(labelSelector)
	ls2 := make(labelSelector)
	ls1["env"] = "PRD"
	ls2["env"] = "TST"
	ls2["peak"] = "P1"
	bdefault := &bucket{1, 1, 1, nil, nil}
	boneselector := &bucket{1, 1, 1, ls1, nil}
	btwoselectors := &bucket{1, 1, 1, ls2, nil}
	if sub := bdefault.subsystem(); sub != "default" {
		t.Errorf("expected default subsystem to be 'default' but got '%s' instead", sub)
	}
	if sub := boneselector.subsystem(); sub != "env:PRD" {
		t.Errorf("expected one-selector subsystem to be 'env:PRD' but got '%s' instead", sub)
	}
	if sub := btwoselectors.subsystem(); sub != "env:TST_peak:P1" {
		t.Errorf("expected two-selectors subsystem to be 'env:TST_peak:P1' but got '%s' instead", sub)
	}
}

func TestMetricInit(t *testing.T) {
	_labels := labels{&label{"env", []string{"PRD", "TST"}}, &label{"customer", []string{"customerA", "customerB"}}}
	_metrics := metrics{
		&metric{"counter", "counter metric", "counter", 1, 50, 10, 1, nil, nil, nil, nil, nil},
		&metric{"gauge", "gauge metric", "gauge", 1, 20, 10, 1, nil, nil, nil, nil, nil},
		&metric{"histogram", "histogram metric", "histogram", 1, 0.300, 10, 80, nil, nil, nil, buckets{
			&bucket{0.1, 0.1, 5, labelSelector{"env": "PRD"}, nil},
			&bucket{0.1, 0.2, 3, labelSelector{"env": "TST"}, nil},
		}, nil},
		&metric{"sum", "sum metric", "summary", 1, 0.300, 10, 80, nil, nil, nil, nil, nil},
	}
	initMetrics(_labels, _metrics)
	if len(_metrics[0].metricVectors) != 1 {
		t.Errorf("expecting 1 counter vector but got %d instead", len(_metrics[0].metricVectors))
	}
	if len(_metrics[1].metricVectors) != 1 {
		t.Errorf("expecting 1 gauge vector but got %d instead", len(_metrics[1].metricVectors))
	}
	if len(_metrics[2].metricVectors) != 2 {
		t.Errorf("expecting 2 histogram vectors but got %d instead", len(_metrics[2].metricVectors))
	}
	if len(_metrics[3].metricVectors) != 1 {
		t.Errorf("expecting 1 summary vector but got %d instead", len(_metrics[3].metricVectors))
	}
	t.Run("Counter", checkMetricLabels(_metrics[0], _labels, dto.MetricType_COUNTER))
	t.Run("Gauge", checkMetricLabels(_metrics[1], _labels, dto.MetricType_GAUGE))
	t.Run("Histogram", checkMetricLabels(_metrics[2], _labels, dto.MetricType_HISTOGRAM))
	t.Run("Summary", checkMetricLabels(_metrics[3], _labels, dto.MetricType_SUMMARY))
}

// each labels combination must be found once in the provided metrics
func checkMetricLabels(m *metric, _labels labels, mtype dto.MetricType) func(t *testing.T) {
	return func(t *testing.T) {
		metricLabels := m.Labels
		wantedLabelNames := m.wantedLabelNames(_labels)
		_metrics := []prometheus.Metric{}
		for _, mvec := range m.metricVectors {
			// cannot collect before having data
			verbosecnt = verbosefrequence
			mvec.refresh(m.Specs)
			_desc := make(chan prometheus.Metric)
			go mvec.collector().Collect(_desc)
		L:
			for {
				time.Sleep(150 * time.Millisecond)
				select {
				case met := <-_desc:
					_metrics = append(_metrics, met)
				default:
					t.Log("all metrics collected")
					close(_desc)
					break L
				}
			}
		}
		lc := _labels.combinations(metricLabels, wantedLabelNames, m.Replicas)
		if len(_metrics) != len(lc.getCombinations()) {
			t.Errorf("number of metrics %d differs from the number of combinations %d", len(_metrics), len(lc.getCombinations()))
		}
		dtoMetrics := make([]*dto.Metric, 0, len(_metrics))
		for i, m := range _metrics {
			desc := m.Desc()
			dtoMetric := &dto.Metric{}
			if err := m.Write(dtoMetric); err != nil {
				t.Fatalf("error collecting metric %v: %s", desc, err)
			}
			if mtype == dto.MetricType_COUNTER && dtoMetric.Counter == nil {
				t.Errorf("metric %d is not of type COUNTER but something else", i)
			} else if mtype == dto.MetricType_GAUGE && dtoMetric.Gauge == nil {
				t.Errorf("metric %d is not of type GAUGE but something else", i)
			} else if mtype == dto.MetricType_HISTOGRAM && dtoMetric.Histogram == nil {
				t.Errorf("metric %d is not of type HISTOGRAM but something else", i)
			} else if mtype == dto.MetricType_SUMMARY && dtoMetric.Summary == nil {
				t.Errorf("metric %d is not of type SUMMARY but something else", i)
			}
			dtoMetrics = append(dtoMetrics, dtoMetric)
		}
		namemap := make(map[string]int)
	LNS:
		for _, lname := range lc.labelNames {
			for i, dtolabel := range dtoMetrics[0].Label {
				if *(dtolabel.Name) == lname {
					namemap[lname] = i
					continue LNS
				}
			}
			t.Fatalf("labelName %s not found in LabelPairs", lname)
		}
	CML:
		for _, com := range lc.getCombinations() {
		CMLI:
			for _, m := range dtoMetrics {
				for i, ln := range lc.labelNames {
					if com[i] != *(m.Label[namemap[ln]].Value) {
						continue CMLI
					}
				}
				continue CML
			}
			t.Errorf("combination %v not found", com)
		}
	}
}

func TestRegistry(t *testing.T) {
	myReg := prometheus.NewRegistry()
	_labels := labels{&label{"env", []string{"PRD", "TST"}}, &label{"customer", []string{"customerA", "customerB"}}}
	_metrics1 := metrics{
		&metric{"counter", "counter metric", "counter", 1, 50, 10, 1, nil, nil, nil, nil, nil},
	}
	_metrics2 := metrics{
		&metric{"counter", "counter metric", "counter", 1, 50, 10, 1, nil, nil, nil, nil, nil},
	}
	initMetrics(_labels, _metrics1)
	initMetrics(_labels, _metrics2)
	if err := registerMetrics(myReg, _metrics1); err != nil {
		t.Errorf("expected no error but got '%s'", err.Error())
	}
	if err := registerMetrics(myReg, _metrics2); err == nil {
		t.Errorf("expected a registry error but nothing")
	} else if err.Error() != "registry error for metric 'counter' vector 0: duplicate metrics collector registration attempted" {
		t.Errorf("expected error 'registry error for metric 'counter' vector 0: duplicate metrics collector registration attempted' but got '%s'", err.Error())
	}
	// a dirty metric should be ignored by the registry
	_ = _metrics2[0].metricVectors[0].dirty(true)
	if err := registerMetrics(myReg, _metrics2); err != nil {
		t.Errorf("expected a no registry for dirty metric but got '%s' instead", err.Error())
	}
}

func TestGetLabelCombinator(t *testing.T) {
	_labels := labels{&label{"env", []string{"PRD", "TST"}}, &label{"customer", []string{"customerA", "customerB"}}}
	ls1 := make(labelSelector)
	ls1["env"] = "PRD"
	s := &spec{100, 5, 10, ls1, fractions{}, new(labelCombinator)}
	s2 := &spec{100, 5, 10, make(labelSelector), fractions{}, new(labelCombinator)}
	expectedCombinations1 := [][]string{
		[]string{"PRD", "customerA", "replica-1"},
		[]string{"PRD", "customerB", "replica-1"},
		[]string{"TST", "customerA", "replica-1"},
		[]string{"TST", "customerB", "replica-1"},
	}
	expectedCombinations2 := [][]string{
		[]string{"PRD", "customerA", "replica-1"},
		[]string{"PRD", "customerB", "replica-1"},
		[]string{"TST", "customerA", "replica-1"},
		[]string{"TST", "customerB", "replica-1"},
		[]string{"PRD", "customerA", "replica-2"},
		[]string{"PRD", "customerB", "replica-2"},
		[]string{"TST", "customerA", "replica-2"},
		[]string{"TST", "customerB", "replica-2"},
	}
	expectedCombinationsSpec := [][]string{
		[]string{"PRD", "customerA", "replica-1"},
		[]string{"PRD", "customerB", "replica-1"},
	}
	expectedCombinationsMetric := [][]string{
		[]string{"TST", "customerA", "replica-1"},
		[]string{"TST", "customerB", "replica-1"},
	}
	lc := _labels.combinations(make(labels, 0), []string{}, 1)
	if len(lc.combinedValues) != 4 {
		t.Errorf("expected combinedValues count of 4 but got %d instead", len(lc.combinedValues))
	} else if ok, missing := arraysInArrays(lc.combinedValues, expectedCombinations1); !ok {
		t.Errorf("expected values don't match, %v missing: %v", missing, lc.combinedValues)
	}
	lc.filter(specs{s2, s})
	if len(s2.labelCombinator.combinedValues) != 2 {
		t.Errorf("default lc expected to contain 2 combinations but got %d instead", len(s2.labelCombinator.combinedValues))
	} else if ok, missing := arraysInArrays(s2.labelCombinator.combinedValues, expectedCombinationsMetric); !ok {
		t.Errorf("expected values don't match, %v missing", missing)
	}
	if len(s.labelCombinator.combinedValues) != 2 {
		t.Errorf("spec's lc expected to contain 2 combinations but got %d instead", len(s.labelCombinator.combinedValues))
	} else if ok, missing := arraysInArrays(s.labelCombinator.combinedValues, expectedCombinationsSpec); !ok {
		t.Errorf("expected values don't match, %v missing", missing)
	}
	lc2 := _labels.combinations(make(labels, 0), []string{}, 2)
	if len(lc2.combinedValues) != 8 {
		t.Errorf("expected combinedValues count of 8 but got %d instead", len(lc2.combinedValues))
	} else if ok, missing := arraysInArrays(lc2.combinedValues, expectedCombinations2); !ok {
		t.Errorf("expected values don't match, %v missing: %v", missing, lc2.combinedValues)
	}
}

func TestBucketsSpecsInit(t *testing.T) {
	_labels := labels{&label{"env", []string{"PRD", "TST"}}, &label{"customer", []string{"customerA", "customerB"}}}
	ls1 := make(labelSelector)
	ls1["customer"] = "customerA"
	s := &spec{100, 5, 10, ls1, fractions{}, new(labelCombinator)}
	s2 := &spec{100, 5, 10, make(labelSelector), fractions{}, new(labelCombinator)}
	lc := _labels.combinations(make(labels, 0), []string{}, 1)
	lc.filter(specs{s2, s})
	b1 := &bucket{0.1, 0.2, 3, labelSelector{"env": "TST"}, nil}
	b2 := &bucket{0.1, 0.2, 3, labelSelector{"env": "PRD"}, nil}
	_buckets := buckets{b1, b2}
	_buckets.createSpecs(specs{s, s2})
	expectedCombinationsB1_1 := [][]string{
		[]string{"TST", "customerA", "replica-1"},
	}
	expectedCombinationsB1_2 := [][]string{
		[]string{"TST", "customerB", "replica-1"},
	}
	expectedCombinationsB2_1 := [][]string{
		[]string{"PRD", "customerA", "replica-1"},
	}
	expectedCombinationsB2_2 := [][]string{
		[]string{"PRD", "customerB", "replica-1"},
	}
	if len(b1.specs) != 2 {
		t.Fatalf("b1 specs' len's supposed to be 2 but got %d instead", len(b1.specs))
	}
	if len(b2.specs) != 2 {
		t.Fatalf("b2 specs' len's supposed to be 2 but got %d instead", len(b2.specs))
	}
	if len(b1.specs[0].labelCombinator.combinedValues) != 1 {
		t.Errorf("b1 s0's lc expected to contain 1 combinations but got %d instead", len(b1.specs[0].labelCombinator.combinedValues))
	} else if ok, missing := arraysInArrays(b1.specs[0].labelCombinator.combinedValues, expectedCombinationsB1_1); !ok {
		t.Errorf("expected b1 s0 values don't match, %v missing", missing)
	} else if len(b1.specs[1].labelCombinator.combinedValues) != 1 {
		t.Errorf("b1 s1's lc expected to contain 1 combinations but got %d instead", len(b1.specs[1].labelCombinator.combinedValues))
	} else if ok, missing := arraysInArrays(b1.specs[1].labelCombinator.combinedValues, expectedCombinationsB1_2); !ok {
		t.Errorf("expected b1 s1 values don't match, %v missing", missing)
	}
	if len(b2.specs[0].labelCombinator.combinedValues) != 1 {
		t.Errorf("b2 s0's lc expected to contain 1 combinations but got %d instead", len(b2.specs[0].labelCombinator.combinedValues))
	} else if ok, missing := arraysInArrays(b2.specs[0].labelCombinator.combinedValues, expectedCombinationsB2_1); !ok {
		t.Errorf("expected b2 s0 values don't match, %v missing", missing)
	} else if len(b2.specs[1].labelCombinator.combinedValues) != 1 {
		t.Errorf("b2 s1's lc expected to contain 1 combinations but got %d instead", len(b2.specs[1].labelCombinator.combinedValues))
	} else if ok, missing := arraysInArrays(b2.specs[1].labelCombinator.combinedValues, expectedCombinationsB2_2); !ok {
		t.Errorf("expected b2 s1 values don't match, %v missing", missing)
	}
}

func TestMetricFind(t *testing.T) {
	m := metrics{&metric{"counter", "counter metric", "counter", 1, 50, 10, 1, nil, nil, nil, nil, nil}}
	m2 := &metric{"counter", "counter metric", "gauge", 1, 50, 10, 1, nil, nil, nil, nil, nil}
	m3 := &metric{"counter", "counter metric", "counter", 1, 50, 10, 1, []string{"env"}, nil, nil, nil, nil}
	_labels := labels{&label{"env", []string{"PRD", "TST"}}, &label{"customer", []string{"customerA", "customerB"}}}
	if m1 := m.findMetric(m[0], _labels); m1 == nil {
		t.Error("findMetric does not find a contained metric")
	}
	if m1 := m.findMetric(m2, _labels); m1 != nil {
		t.Error("findMetric matches metric of different type")
	}
	if m1 := m.findMetric(m3, _labels); m1 != nil {
		t.Error("findMetric matches metric with different labels")
	}
}

func TestMetricDefaultMerge(t *testing.T) {
	_labels := labels{&label{"env", []string{"PRD", "TST"}}, &label{"customer", []string{"customerA", "customerB"}}}
	m1 := metrics{
		&metric{"counter", "counter metric", "counter", 1, 50, 10, 1, nil, nil, nil, nil, nil},
		&metric{"gauge", "gauge metric", "gauge", 1, 50, 10, 1, []string{"env"}, nil, nil, nil, nil},
		&metric{"summary", "summary metric", "summary", 1, 50, 10, 1, nil, nil, nil, nil, nil},
	}
	m2 := metrics{
		&metric{"gauge", "gauge metric", "gauge", 1, 50, 10, 1, []string{"customer"}, nil, nil, nil, nil},
		&metric{"counter", "counter metric", "counter", 1, 50, 10, 1, nil, nil, nil, nil, nil},
		&metric{"summary2", "summary metric", "summary", 1, 50, 10, 1, nil, nil, nil, nil, nil},
	}
	initMetrics(_labels, m1)
	initMetrics(_labels, m2)
	m1counter := m1[0].metricVectors[0]
	mergeMetrics(m2, m1, _labels)
	if len(m1[0].metricVectors) > 0 {
		t.Errorf("expected counter vector to be merged, but it still exists on the old metric")
	}
	if len(m1[1].metricVectors) == 0 {
		t.Errorf("expected gauge vector NOT to be merged, but it does not exist on the old metric anymore")
	}
	if len(m1[2].metricVectors) == 0 {
		t.Errorf("expected summary vector NOT to be merged, but it does not exist on the old metric anymore")
	}
	if len(m2[0].metricVectors) != 1 {
		t.Errorf("expected exactly one new gauge vector, but have %d", len(m2[0].metricVectors))
	}
	if len(m2[1].metricVectors) != 1 {
		t.Errorf("expected exactly one new counter vector, but have %d", len(m2[0].metricVectors))
	} else if m2[1].metricVectors[0] != m1counter {
		t.Errorf("expected the counter metric to be merged, but found new instance")
	} else if !m2[1].metricVectors[0].dirty(false) {
		t.Errorf("expected merged counter metric to be dirty, but it isn't")
	}
	if len(m2[2].metricVectors) != 1 {
		t.Errorf("expected exactly one new summary vector, but have %d", len(m2[0].metricVectors))
	}
}

func TestMetricMergeValueChange(t *testing.T) {
	myReg := prometheus.NewRegistry()
	_labels := labels{&label{"env", []string{"PRD"}}, &label{"customer", []string{"customerA", "customerB"}}}
	m1 := metrics{
		&metric{"gauge", "gauge metric", "gauge", 1, 1, 0, 1, []string{"env"}, nil, nil, nil, nil},
	}
	m2 := metrics{
		&metric{"gauge", "gauge metric", "gauge", 1, 0, 0, 1, []string{"env"}, nil, nil, nil, nil},
	}
	initMetrics(_labels, m1)
	registerMetrics(myReg, m1)
	m1[0].metricVectors[0].refresh(m1[0].Specs)
	mfs1, err1 := myReg.Gather()
	if err1 != nil {
		t.Errorf("mfs1 gather error: %s", err1.Error())
		return
	}
	if len(mfs1) != 1 {
		t.Errorf("expected one metric family but found %d instead", len(mfs1))
	}
	if len(mfs1[0].Metric) != 1 {
		t.Errorf("expected one metric but found %d instead", len(mfs1[0].Metric))
	}
	val1 := mfs1[0].Metric[0].GetGauge().GetValue()
	if val1 != 1.0 {
		t.Errorf("expected val1 to be 1.0 but got %f instead", val1)
	}
	initMetrics(_labels, m2)
	mergeMetrics(m2, m1, _labels)
	registerMetrics(myReg, m2)
	m2[0].metricVectors[0].refresh(m2[0].Specs)
	mfs2, err2 := myReg.Gather()
	if err2 != nil {
		t.Errorf("mfs2 gather error: %s", err2.Error())
		return
	}
	if len(mfs2) != 1 {
		t.Errorf("expected one metric family but found %d instead", len(mfs2))
	}
	if len(mfs2[0].Metric) != 1 {
		t.Errorf("expected one metric but found %d instead", len(mfs2[0].Metric))
	}
	val2 := mfs2[0].Metric[0].GetGauge().GetValue()
	if val2 != 0.0 {
		t.Errorf("expected val2 to be 0.0 but got %f instead", val2)
	}
}

func TestBucketFind(t *testing.T) {
	ls1 := make(map[string]string)
	ls2 := make(map[string]string)
	ls1["env"] = "PRD"
	ls2["env"] = "TST"
	b1 := &bucket{1, 1, 1, ls1, nil}
	b2 := &bucket{1, 2, 1, ls2, nil}
	b3 := &bucket{1, 1, 1, nil, nil}
	b := buckets{b1, b2, b3}
	f1 := b.find(b1)
	f2 := b.find(b2)
	f3 := b.find(b3)
	if f1 != b1 {
		t.Error("f1 != b1")
	}
	if f2 != b2 {
		t.Error("f2 != b2")
	}
	if f3 != b3 {
		t.Error("f3 != b3")
	}
}

func TestMetricHistogramMerge(t *testing.T) {
	_labels := labels{&label{"env", []string{"PRD", "TST"}}, &label{"customer", []string{"customerA", "customerB"}}}
	ls1 := make(map[string]string)
	ls2 := make(map[string]string)
	ls3 := make(map[string]string)
	ls1["env"] = "PRD"
	ls2["env"] = "TST"
	ls3["env"] = "DEV"
	b1 := &bucket{1, 1, 1, ls1, nil}
	b2 := &bucket{1, 2, 1, ls2, nil}
	b3 := &bucket{1, 1, 1, nil, nil}
	bs1 := buckets{b1, b2, b3}
	b1a := &bucket{2, 1, 1, ls1, nil}
	b2a := &bucket{1, 2, 1, ls3, nil}
	b3a := &bucket{1, 1, 1, nil, nil}
	bs2 := buckets{b1a, b2a, b3a}
	m1 := metrics{
		&metric{"histogram1", "histogram metric", "histogram", 1, 50, 10, 1, nil, nil, nil, bs1, nil},
	}
	m2 := metrics{
		&metric{"histogram1", "histogram metric", "histogram", 1, 50, 10, 1, nil, nil, nil, bs2, nil},
	}
	initMetrics(_labels, m1)
	initMetrics(_labels, m2)
	m1v3 := m1[0].metricVectors[2].(*metricHistogramVec)
	m1v3object := m1v3.HistogramVec
	mergeMetrics(m2, m1, _labels)
	if len(m1[0].metricVectors) != 2 {
		t.Errorf("expected 2 vectors of the old metric to have remained but got %d instead", len(m1[0].metricVectors))
	}
	if v1, ok := m2[0].metricVectors[0].(*metricHistogramVec); !ok {
		t.Errorf("vector[0] not of type *metricHistogramVec")
	} else {
		if v1.bucket != b1a {
			t.Errorf("v1's bucket does not match the right object")
		}
	}
	if v2, ok := m2[0].metricVectors[1].(*metricHistogramVec); !ok {
		t.Errorf("vector[1] not of type *metricHistogramVec")
	} else {
		if v2.bucket != b2a {
			t.Errorf("v2's bucket does not match the right object")
		}
	}
	if v3, ok := m2[0].metricVectors[2].(*metricHistogramVec); !ok {
		t.Errorf("vector[2] not of type *metricHistogramVec")
	} else {
		if v3.HistogramVec != m1v3object {
			t.Errorf("vector[2]'s HistogramVec should have been copied from old metric but wasn't; old: %v; new: %v", m1v3object, v3.HistogramVec)
		}
		if !v3.dirty(false) {
			t.Errorf("verctor[2] should be dirty but is not")
		}
	}
}

func TestFractionValues(t *testing.T) {
	_labels := labels{&label{"env", []string{"PRD", "TST"}}, &label{"customer", []string{"customerA", "customerB"}}}
	ls1 := make(map[string]string)
	ls2 := make(map[string]string)
	ls1["env"] = "PRD"
	ls2["env"] = "TST"
	fPRD := fractions{
		&fraction{30, 10, 1, 10},
		&fraction{60, 20, 1, 20},
	}
	fTST := fractions{
		&fraction{10, 50, 1, 50},
		&fraction{50, 50, 1, 50},
	}
	sPRD := &spec{100, 25, 1, ls1, fPRD, &labelCombinator{}}
	sTST := &spec{1, 0, 1, ls2, fTST, &labelCombinator{}}
	_specs := specs{
		sPRD,
		sTST,
	}
	_metrics := metrics{
		&metric{"summary", "summary metric", "summary", 10, 50, 10, 1, nil, nil, _specs, nil, nil},
	}
	initMetrics(_labels, _metrics)
	valuesPRD := make([]float64, 0, 20)
	valuesTST := make([]float64, 0, 20)
	labelsPRD := sPRD.getCombinations()
	for i := range labelsPRD {
		valuesPRD = append(valuesPRD, sPRD.refreshValues(10, uint(i+1), uint(len(labelsPRD)))...)
	}
	labelsTST := sTST.getCombinations()
	for i := range labelsTST {
		valuesTST = append(valuesTST, sTST.refreshValues(10, uint(i+1), uint(len(labelsTST)))...)
	}
	if len(valuesPRD) != 20 {
		t.Errorf("expected 20 PRD values but got %d instead", uint(len(valuesPRD)))
	}
	if len(valuesTST) != 20 {
		t.Errorf("expected 20 TST values but got %d instead", uint(len(valuesTST)))
	}
	t.Run("PRD1", checkSpecValues(valuesPRD[0:2], 27.0, 33.0))
	t.Run("PRD2", checkSpecValues(valuesPRD[2:6], 48.0, 72.0))
	t.Run("PRD3", checkSpecValues(valuesPRD[6:], 75.0, 125.0))
	t.Run("TST1", checkSpecValues(valuesTST[0:10], 5.0, 15.0))
	t.Run("TST2", checkSpecValues(valuesTST[10:], 25.0, 75.0))
}

func checkSpecValues(values []float64, min, max float64) func(t *testing.T) {
	return func(t *testing.T) {
		for i, v := range values {
			if v < min || v > max {
				t.Errorf("value %d out of range: %f should be >%f and <%f", i, v, min, max)
			}
		}
	}
}

func arraysInArrays(t, s [][]string) (bool, []string) {
	_t := make([][]string, 0)
	_s := make([][]string, 0)
	for _, a := range t {
		_a := append([]string{}, a...)
		sort.Strings(_a)
		_t = append(_t, _a)
	}
	for _, a := range s {
		_a := append([]string{}, a...)
		sort.Strings(_a)
		_s = append(_s, _a)
	}
O:
	for _, a := range _s {
		for _, i := range _t {
			if strings.Join(a, ",") == strings.Join(i, ",") {
				continue O
			}
		}
		return false, a
	}
	return true, nil
}
